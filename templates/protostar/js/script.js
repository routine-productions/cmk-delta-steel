(function($){
    $(document).ready(function(){

        $(".Order").click(function(){
            $(".Wrapper-Order-Block").fadeIn();
        });

        $(".Layout").click(function(){
            $(".Wrapper-Order-Block").fadeOut();
        });



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Send Mail By Ajax
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $('.Head-Form .Call-Me').click(function ()
        {

            //Get Data
            var Phone = $('.Head-Form [name=phone]').val();


            // Set Content
            var Query = 'phone=' + Phone;

            // Validate Email

            if (Phone.length > 0)
            {
                $.ajax({
                    url:     '/actions/connect.php',
                    type:    'POST',
                    data:    Query,
                    success: function (Data)
                    {
                        $('.Registration-Success, .Registration-Error').remove();

                        $('.Head-Form .Call-Me').after(Data);

                        $(".Wrapper-Order-Block").delay(2000).fadeOut();

                    },
                    error:   function ()
                    {
                        $('.Registration-Success, .Registration-Error').remove();

                        $('.Head-Form .Call-Me').after('<div class="Registration-Error">Ошибка, попробуйте позже.</div>');
                    }
                });
            } else
            {
                $('.Registration-Success, .Registration-Error').remove();
                $('.Head-Form .Call-Me').after('<div class="Registration-Error">Введите телефон!</div>');
            }

            return false;
        });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        $(window).load(function () {
            $('.Red-Kran-Logo').css('marginLeft','-' + ($('.Red-Kran-Logo').width()/2+20) + 'px');
            $('.Red-Kran-Logo').animate({'top': '-' + ($('.Red-Kran-Logo').height() - $('.Red-Kran').height()) + 'px'}, 2000);
        });

        $(document).ready(function () {

            $(window).resize(function(){
                $('.Red-Kran-Logo').css('marginLeft','-' +($('.Red-Kran-Logo').width()/2+20) + 'px');
                $('.Red-Kran-Logo').animate({'top': '-' + ($('.Red-Kran-Logo').height() - $('.Red-Kran').height()) + 'px'}, 2000);
            });

            $('#map_canvas1').addClass('scrolloff');
            $('#contacts').on('click', function () {
                $('#map_canvas1').removeClass('scrolloff');
            });
            $("#map_canvas1").mouseleave(function () {
                $('#map_canvas1').addClass('scrolloff');
            });
        });


    });

})(jQuery);
