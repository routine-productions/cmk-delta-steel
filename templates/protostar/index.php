<!DOCTYPE html>

<html>
<head>
    <title>СМК ДЕЛЬТА металлоконструкции</title>
    <jdoc:include type="head" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" type="image/png" href="/favicon.png" />
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="/templates/protostar/js/script.js"></script>
    <script src="/templates/protostar/js/jquery.navigation.js"></script>
    <link rel="stylesheet" href="/templates/protostar/css/style.css"/>
    <link rel="stylesheet" href="/templates/protostar/css/media.css"/>
    <link href='http://fonts.googleapis.com/css?family=Exo+2:400,300,500,600&subset=cyrillic,latin' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="Wrapper">
    <div class="Header">
        <div class="Head-Menu">
            <span><a href="#main">ГЛАВНАЯ</a></span>
            <span><a href="#advantages">ПРЕИМУЩЕСТВА</a></span>
            <span><a href="#about">О КОМПАНИИ</a></span>
            <span><a href="#services">УСЛУГИ</a></span>
            <span><a href="#recommendations">РЕКОМЕНДАЦИИ</a></span>
            <span><a href="#contacts">КОНТАКТЫ</a></span>
        </div>
        <div class="Head-Menu-Sub" id="main">
            <div class="Head-Delta-Logo">
<!--                <p class="Delta-Logo-Text-1">ДЕЛЬТА</p>-->
<!--                <p class="Delta-Logo-Text-2">металлоконструкции</p>-->
                <img src="/images/red-logo-kran.png" alt="" class="Red-Kran-Logo" style=""/>
                <img src="/images/red-logo.png" class="Red-Kran"/>
            </div>
            <div class="Head-Center-Text">
                <p>металлоконструкции для всех
                    отраслей промышленности
                    и гражданского строительства</p>
            </div>
            <div class="Head-Phone">
                <p class="Phone-Number">
                    <img src="/ico/phone-head.png" alt=""/>
                    (342) 256-56-93</p>
                <button class="Order">Заказать обратный звонок</button>
            </div>
        </div>
    </div>


    <div class="Advantage-Wrapper" id="advantages">
        <div class="Advantage">
            <span class="Advantage-Head">наши преимущества</span>
            <div class="Advantage-Block">
                <div class="Wallet">
                    <img src="/ico/wallet.png" alt=""/>
                    <p>Высокий темп выпуска</p>
                    <p>готовой продукции</p>
                </div>
                <div class="User">
                    <img src="/ico/user.png" alt=""/>
                    <p>Обученный и аттестованный</p>
                    <p>персонал</p>
                </div>
                <div class="Factory">
                    <img src="/ico/factory.png" alt=""/>
                    <p>Современное оснащение</p>
                    <p>и прикладное оборудование</p>
                </div>
                <div class="Concrete">
                    <img src="/ico/concrete.png" alt=""/>
                    <p>Производственная площадка</p>
                    <p>c открытыми и закрытыми складскими</p>
                    <p>помещениями универсального назначения</p>
                </div>
                <div class="Concrete">
                    <img src="/ico/concrete-2.png" alt=""/>
                    <p>Контроль качества продукции</p>
                    <p>производится лабораторией</p>
                    <p>неразрушающего контроля</p>
                </div>
                <div class="Concrete">
                    <img src="/ico/concrete-3.png" alt=""/>
                    <p>Высокотоннажное механизированное</p>
                    <p>подъёмное оборудование</p>
                </div>
            </div>
        </div>
    </div>


    <div class="By-Company" id="about">
        <span class="By-Company-Head">
            О Компании
        </span>
        <div class="By-Company-Block-Wrapper">
            <div class="By-Company-Picture">
                <img src="/images/two-builder.png" alt=""/>
            </div>
            <div class="By-Company-Text">
                <p class="By-Company-Description">
                    Наша компания выступает в роли строительно–монтажной
                    организации готовых технологий для производств в различных
                    направлениях нефтяной, газовой и химической отрасли.
                    Произведенные нами работы объединены по общему принципу
                    — «надежность и высокая степень качества».
                </p>
                <p class="By-Company-Description">СМК Дельта оказывает услуги по строительству,
                    ремонту и сервисному обслуживанию технологического оборудования и технических
                    трубопроводов предприятий химической, нефтехимической и нефтедобывающей промышленности.
                </p>
                <p class="By-Company-Description">
                    В штате нашей организации высококвалифицированные специалисты:
                    ИТР высшей категории, аттестованные монтажники и сварщики.
                </p>
            </div>
        </div>
    </div>

    <div class="Our-Projects" id="services">
        <p class="Our-Projects-Head">услуги</p>
        <div class="Our-Projects-Parts">
            <div class="Our-Projects-Part-Blocks">
                <div class="Our-Projects-Part Part-1">
                    <div class="Our-Projects-Picture">
                        <div class="Our-Projects-Picture-Number"><p>1</p></div>
                        <div class="Our-Projects-Picture-Text">
                            <p>Изготовление металлических</p>
                            <p>строительных конструкций</p>
                        </div>
                    </div>
                    <div class="Our-Projects-Description">
                        <p class="Our-Projects-Text-1">
                            Производство сборных строительных конструкций зданий
                            из металла: подсобных помещений на
                            строительных площадках, металлических несущих
                            конструкций зданий и сооружений,
                            металлических несущих конструкций промышленных
                            сооружений и оборудования, монтаж
                            строительных металлических конструкций собственного производства.
                        </p>
                    </div>
                </div>
                <div class="Our-Projects-Part Part-2">
                    <div class="Our-Projects-Picture">
                        <div class="Our-Projects-Picture-Number"><p>2</p></div>
                        <div class="Our-Projects-Picture-Text">
                            <p>Изготовление металлоконструкций,</p>
                            <p>поддерживающих технологические</p>
                            <p>трубопроводы</p>
                        </div>
                    </div>
                    <div class="Our-Projects-Description">
                        <p class="Our-Projects-Text-1">На предприятии выполняется производство опорныых конструкций
                            для трубопроводных систем, изготавливаемых из стали.
                            Опоры играют важнейшую роль в обеспечении длительной бесперебойной работы трубопроводной системы,
                            являются конструкционным элементом, способствующим улучшению безопасности эксплуатации трубопроводов.
                        </p>
                    </div>
                </div>
                <div class="Our-Projects-Part Part-3">
                    <div class="Our-Projects-Picture">
                        <div class="Our-Projects-Picture-Number"><p>3</p></div>
                        <div class="Our-Projects-Picture-Text">
                            <p>Изготовление частей трубо-</p>
                            <p>проводов любой сложности</p>
                        </div>
                    </div>
                    <div class="Our-Projects-Description">
                        <p class="Our-Projects-Text-1">
                            В номенклатуру нашего производства входят все разновидности ныне существующих стандартных соединительных деталей:
                            отводы сварные, отводы гнутые, отводы крутоизогнутые,
                            тройники сварные, тройники штампосварные, тройники бесшовные.
                        </p>
                    </div>
                </div>
                <div class="Our-Projects-Part Part-4">
                    <div class="Our-Projects-Picture">
                        <div class="Our-Projects-Picture-Number"><p>4</p></div>
                        <div class="Our-Projects-Picture-Text">
                            <p>Выполнение обработки</p>
                            <p>металлических конструкций</p>
                        </div>
                    </div>
                    <div class="Our-Projects-Description">
                        <p class="Our-Projects-Text-1">На предприятии выполняется антикоррозийная,
                            огнезащитная, а такаже пескоструйная
                            (очистка металлических заготовок от окалины, старой краски, ржавчины и других загрязнений
                            обезжиривание металлических заготовок перед окраской, газотермическим напылением, гальванотехническими и т. п. операциями и т.д.)
                            обработка металлических конструкций.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="Our-Clients-Wrapper" id="recommendations">
        <div class="Our-Clients">
            <p class="Our-Clients-Head">
                Рекомендательные письма наших клиентов
            </p>
            <div class="Client">
                <a data-lightbox="group:clients" href="/images/docs-1.jpg"><img src="/images/docs-1.jpg" alt="" /></a>
                <a data-lightbox="group:clients" href="/images/docs-2.jpg"><img src="/images/docs-2.jpg" alt="" /></a>
                <a data-lightbox="group:clients" href="/images/docs-3.jpg"><img src="/images/docs-3.jpg" alt="" /></a>

            </div>
        </div>
    </div>


    <div class="Contacts" id="contacts">
        <iframe id="map_canvas1" class="Map" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2118.4905463025693!2d56.1399496!3d57.9286995!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43e8be3c74d15a9f%3A0x6c542354a33fe9c8!2z0KHQnNCaICLQlNCV0JvQrNCi0JAiLCDQntCe0J4!5e0!3m2!1sru!2sru!4v1428438310995"></iframe>
        <div class="Contacts-Text">
            <p class="Contacts-Text-Head">контактная информация</p>
            <div class="Contacts-Info">
                <p>Почтовый адрес:</p><span>614065, г. Пермь, ул. Промышленная, 101</span>
            </div><br>
            <div class="Contacts-Info">
                <p>Юридический адрес:</p><span>614065, г. Пермь, ш.Космонавтов, 304</span><br>
            </div>
            <div class="Contacts-Info">
                <p>Тел./факс:</p><span>(342) 256-56-93</span><br>
            </div>
            <div class="Contacts-Info">
                <p>e–mail:</p><span><a href="mailto:office@cmkdelta.ru">office@cmkdelta.ru</a></span>
            </div>

            <p class="Contacts-Text-Head-2">Руководство Компании</p>
            <div class="Leader-1">
                <p class="Leader-Head">ЗЕЛЕНКИН СЕРГЕЙ АЛЕКСАНДРОВИЧ</p>
                <p class="Leader-Post">Генеральный директор</p>
                <p class="Email"><a href="mailo:s.zelenkin@cmkdelta.ru">s.zelenkin@cmkdelta.ru</a></p>
            </div>
            <div class="Leader">
                <p class="Leader-Head">ВЕСЕЛИЦКИЙ АЛЕКСАНДР ВЯЧЕСЛАВОВИЧ</p>
                <p class="Leader-Post">Первый заместитель генерального директора, главный инженер</p>
                <p class="Email"><a href="mailto:a.veselitskiy@cmkdelta.ru">a.veselitskiy@cmkdelta.ru</a></p>
            </div>
            <div class="Leader">
                <p class="Leader-Head">КОРОЛЁВА ЛЮБОВЬ ВАСИЛЬЕВНА</p>
                <p class="Leader-Post">Заместитель генерального директора, главный бухгалтер</p>
                <p class="Email"><a href="mailto:l.koroleva@cmkdelta.ru">l.koroleva@cmkdelta.ru</a></p>
            </div>
        </div>
    </div>


    <div class="Our-Clients">
            <span class="Our-Clients-Head">
                наши клиенты
            </span>
        <div class="Client">
            <img src="/ico/Client-1.jpg" alt=""/>
            <img src="/ico/Client-2.jpg" alt=""/>
            <img src="/ico/Client-3.jpg" alt=""/>
            <img src="/ico/Client-4.jpg" alt=""/>
            <img src="/ico/Client-5.jpg" alt=""/>
            <img src="/ico/Client-6.jpg" alt=""/>
            <img src="/ico/Client-7.jpg" alt=""/>
        </div>
    </div>


    <div class="Footer">
        <div class="Head-Delta-Logo">
<!--            <p class="Delta-Logo-Text-1">ДЕЛЬТА</p>-->
<!--            <p class="Delta-Logo-Text-2">металлоконструкции</p>-->
            <img src="/images/logo-bottom.png"/>
        </div>
        <p class="Footer-Text">Copyright © 2015 | Общество с ограниченной ответственностью "СМК "ДЕЛЬТА"</p>
    </div>
</div>


<div class="Wrapper-Order-Block">
    <div class="Layout"></div>
    <div class="Order-Block">
        <p class="Order-Block-Head">Заказать обратный звонок</p>
        <p class="Your-Phone">Ваш телефон:</p>

        <form class="Head-Form" action="">
            <input type="text" name="phone">
            <button class="Call-Me">Перезвоните мне</button>
        </form>
    </div>
</div>
</body>
</html>